package com.jcs.jcslib;

import android.util.Log;

public class TestClass {
    public void LogNativeAndroidLogcatMessage() {
        Log.d("Unity", "Native logcat Message!");
    }

    public int add(int a, int b)  {
        return a + b;
    }
}
